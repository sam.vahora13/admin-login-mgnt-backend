const express = require('express')
const app = express()
var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
var admin = require('firebase-admin');
// var serviceAccount = require("./server-files/social-login-mgnt/react-native-login-mgnt-firebase-adminsdk-rqg3y-233c8c77c8.json");
var serviceAccount = require("./server-files/socialLoginH/sociallogin-eeeb6-firebase-adminsdk-uzgv0-2e4e93f94f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://react-native-login-mgnt.firebaseio.com"
});
const port = 3010
app.listen(port, () => console.log(`Example app listening on port ${port}!`))



app.get('/', (req, res) => res.send('Hello World!'))

app.get('/getAllUser',(req,res) => {

    let allUser = []

    const listAllUsers = (nextPageToken) => {
        // List batch of users, 1000 at a time.
        admin.auth().listUsers(1000, nextPageToken)
          .then((listUsersResult) => {
            listUsersResult.users.forEach(function(userRecord) {
                console.log("userRecord >>",userRecord);
                // res.status(200).send({success: true, result: {data:  userRecord.toJSON()} })
                let userdata = {
                    id : userRecord.uid,
                    email : userRecord.email,
                    providerId : (userRecord.providerData[0].providerId == "password") ? "Custom-Email" : userRecord.providerData[0].providerId
                }
                console.log("userdata >>",userdata);
                allUser.push(userdata);
                
            //   console.log('user', userRecord.toJSON());
            });
            if (listUsersResult.pageToken) {
                console.log("listUsersResult.pageToken >>",listUsersResult.pageToken);
              // List next batch of users.
              listAllUsers(listUsersResult.pageToken);
            }else{
                res.status(200).send(allUser)
            }
          })
          .catch((error) => {
            // res.status(400).send({success: false, result: {error: error} })
            console.log('Error listing users:', error);
          });
      }

    listAllUsers();


})
app.get('/getUser',(req,res) => {

    admin.auth().getUser("GEN6TwiQF0YfTcEYvECfNPAMjVU2")
    .then(function(userRecord) {
      // See the UserRecord reference doc for the contents of userRecord.
      res.status(200).send({success: true, result: {data:  userRecord.toJSON()} })
      console.log('Successfully fetched user data:', userRecord.toJSON());
    })
    .catch(function(error) {
      console.log('Error fetching user data:', error);
      res.status(400).send({success: false, result: {error: error} })
    });


})

